About
-----------
Bear Hunt provides a quick and powerful Solr Search API bundle that sets up a Solr Server and a default node index. It also automatically configures a faceted Views search pane right out of the box and places the view and facets at /search within a panel.

Instructions
-----------
View INSTALL.txt for specific instructions.
